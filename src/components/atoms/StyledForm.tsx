import styled from 'styled-components'

const StyledForm = styled.form`
  label {
    display: block;
    margin-bottom: 1rem;
    span {
      display: block;
      margin-bottom: 5px;
    }
  }

  input,
  textarea,
  select {
    padding: 5px;
    border-radius: 3px;
    border: 1px inset #eee;
    &[aria-invalid='true'] {
      background-color: rgba(255, 0, 0, 0.1);
    }
  }

  button {
    padding: 5px 10px;
    border-radius: 3px;
  }

  p[role='alert'] {
    color: red;
    margin-top: 0;
    font-size: 0.8em;
  }
`
export default StyledForm
