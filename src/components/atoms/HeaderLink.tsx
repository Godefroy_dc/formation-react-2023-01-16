import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const StyledLi = styled.li`
  margin-right: 1em;

  a {
    text-decoration: none;
    padding: 0.5em 1em;
    color: #000;
    border: 1px solid #ccc;
    border-radius: 0.5em;
    background-color: #f2f4fc;
    &:hover {
      background-color: #dbe0f3;
    }
    &.active {
      border-color: #000;
    }
  }
`

interface Props {
  to: string
  children: string
}

export function HeaderLink({ to, children }: Props) {
  return (
    <StyledLi>
      <NavLink
        to={to}
        className={({ isActive }) => (isActive ? 'active' : undefined)}
      >
        {children}
      </NavLink>
    </StyledLi>
  )
}
