import { useContext } from 'react'
import { FaMoon, FaSun } from 'react-icons/fa'
import { ThemeContext } from '../../contexts/ThemeContext'

export function ThemeSwitch() {
  const { theme, setTheme } = useContext(ThemeContext)

  return (
    <button
      style={{ float: 'right' }}
      onClick={() => setTheme(theme === 'light' ? 'dark' : 'light')}
    >
      {theme === 'light' ? (
        <>
          <FaMoon /> Dark
        </>
      ) : (
        <>
          <FaSun /> Light
        </>
      )}
    </button>
  )
}
