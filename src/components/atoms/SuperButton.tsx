import { memo } from 'react'
import { IconType } from 'react-icons'

interface Props {
  icon: IconType | JSX.Element
  children: string
  onClick: () => void
}

const style = {
  color: 'blue',
  borderColor: 'blue',
}

function SuperButton({ icon: Icon, children, onClick }: Props) {
  return (
    <button style={style} onClick={onClick}>
      {typeof Icon === 'function' ? <Icon /> : Icon} {children}
    </button>
  )
}

export default memo(SuperButton)
