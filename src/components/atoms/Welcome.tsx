interface Props {
  name: string
}

export default function Welcome(props: Props) {
  return <h1>Bonjour {props.name}</h1>
}
