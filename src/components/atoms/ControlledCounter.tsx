interface Props {
  value: number
  onChange: (count: number) => void
}

export default function ControlledCounter({ value, onChange }: Props) {
  return (
    <div>
      Compteur : {value}
      <button onClick={() => onChange(value - 1)}>-1</button>
      <button onClick={() => onChange(value + 1)}>+1</button>
    </div>
  )
}
