import styled from 'styled-components'

export const StyledPokemonCard = styled.div`
  width: 250px;
  height: 349px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
  border-radius: 15px;
  padding: 1em;
  background-color: #fff596;
  text-align: center;
  transition: all 0.2s ease-in-out;

  &:hover {
    transform: scale(1.03);
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
  }

  h1 {
    font-size: 1.3em;
  }
  img {
    width: 200px;
    height: 200px;
  }
  .info {
    display: flex;
    justify-content: space-between;
  }
`
