import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../store'
import { decrement, increment } from '../../store/counter'

export default function ReduxCounter() {
  const count = useSelector((state: RootState) => state.counter.value)
  const dispatch = useDispatch()

  return (
    <div>
      Compteur : {count}
      <button onClick={() => dispatch(decrement())}>-1</button>
      <button onClick={() => dispatch(increment())}>+1</button>
    </div>
  )
}
