import { useEffect, useState } from 'react'

interface Props {
  onChange: (count: number) => void
}

export default function Counter({ onChange }: Props) {
  const [count, setCount] = useState(0)

  useEffect(() => {
    console.log('[Counter] Mounted')
    return () => {
      console.log('[Counter] Unmounted')
    }
  }, [])

  useEffect(() => {
    onChange(count)
    console.log('[Counter] New:', count)
    return () => {
      console.log('[Counter] Old:', count)
    }
  }, [count])

  return (
    <div>
      Compteur : {count}
      <button onClick={() => setCount((c) => c - 1)}>-1</button>
      <button onClick={() => setCount((c) => c + 1)}>+1</button>
    </div>
  )
}
