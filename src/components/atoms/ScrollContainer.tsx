import { useRef } from 'react'

// Example of ref usage to scroll in a container

export default function ScrollContainer() {
  const containerRef = useRef<HTMLDivElement>(null)

  const scrollToTop = () => {
    if (!containerRef.current) return
    containerRef.current.scrollTo(0, 0)
  }

  const scrollToBottom = () => {
    if (!containerRef.current) return
    containerRef.current.scrollTo(0, containerRef.current.scrollHeight)
  }

  return (
    <>
      <div
        ref={containerRef}
        style={{
          width: 300,
          height: 150,
          overflow: 'auto',
          border: '1px solid #ccc',
        }}
      >
        {range(0, 100).map((i) => (
          <div key={i}>{i}</div>
        ))}
      </div>

      <button onClick={scrollToTop}>Scroll to top</button>
      <button onClick={scrollToBottom}>Scroll to bottom</button>
    </>
  )
}

// Array range
const range = (start: number, end: number) => {
  const length = end - start
  return Array.from({ length }, (_, i) => start + i)
}
