import styled from 'styled-components'
import { HeaderLink } from '../atoms/HeaderLink'
import { ThemeSwitch } from '../atoms/ThemeSwitch'

const StyledHeader = styled.header`
  border-bottom: 1px solid #ccc;
  padding: 2em;

  h1 {
    margin: 0 0 1em 0;
    font-size: 1.8em;
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;

    li {
      margin-right: 1em;

      a {
        text-decoration: none;
        padding: 0.5em 1em;
        color: #000;
        border: 1px solid #ccc;
        border-radius: 0.5em;
        background-color: #f2f4fc;
        &:hover {
          background-color: #dbe0f3;
        }
        &.active {
          border-color: #000;
        }
      }
    }
  }
`

export function Header() {
  return (
    <StyledHeader>
      <ThemeSwitch />
      <h1>Webapp React</h1>
      <ul>
        <HeaderLink to="/">Accueil</HeaderLink>
        <HeaderLink to="/tests">Tests</HeaderLink>
        <HeaderLink to="/pokemons">Pokemons</HeaderLink>
        <HeaderLink to="/signin">Connexion</HeaderLink>
        <HeaderLink to="/signup">Inscription</HeaderLink>
      </ul>
    </StyledHeader>
  )
}
