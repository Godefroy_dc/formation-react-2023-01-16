import { usePokemon } from '../../hooks/usePokemon'
import { StyledPokemonCard } from '../atoms/StyledPokemonCard'

interface Props {
  id: number
}

export default function PokemonCard({ id }: Props) {
  const { data, error, loading } = usePokemon(id)

  return (
    <StyledPokemonCard>
      {loading && <p>Chargement...</p>}

      {error && <p>{error}</p>}

      {data && (
        <>
          <h1>{data.name}</h1>
          <img src={data.sprites.front_default} alt="" />
          <p className="info">
            <span>N°{data.id}</span>
            <span>Taille: {data.height}m</span>
            <span>Poids: {data.weight}kg</span>
          </p>
        </>
      )}
    </StyledPokemonCard>
  )
}
