import { FaPause, FaPlay, FaStop } from 'react-icons/fa'
import { useChrono } from '../../hooks/useChrono'
import SuperButton from '../atoms/SuperButton'

export default function Chrono() {
  const { ms, play, pause, stop } = useChrono()

  return (
    <div>
      Chrono : {Math.round(ms / 100) / 10}s
      <div>
        <SuperButton icon={FaPlay} onClick={play}>
          Play
        </SuperButton>
        <SuperButton icon={FaPause} onClick={pause}>
          Pause
        </SuperButton>
        <SuperButton icon={FaStop} onClick={stop}>
          Stop
        </SuperButton>
      </div>
    </div>
  )
}
