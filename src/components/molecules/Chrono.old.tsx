import { useCallback, useEffect, useRef, useState } from 'react'

function useChrono() {
  const [seconds, setSeconds] = useState(0)
  const intervalRef = useRef(0)

  const handlePlay = useCallback(() => {
    if (intervalRef.current) return
    intervalRef.current = setInterval(() => {
      setSeconds((s) => s + 1)
    }, 1000)
  }, [])

  const handlePause = useCallback(() => {
    clearInterval(intervalRef.current)
    intervalRef.current = 0
  }, [])

  const handleStop = useCallback(() => {
    clearInterval(intervalRef.current)
    intervalRef.current = 0
  }, [])

  // Clear interval at unmount
  useEffect(() => () => clearInterval(intervalRef.current), [])

  return {
    seconds,
    handlePlay,
    handlePause,
    handleStop,
  }
}

export default function Chrono() {
  const { seconds, handlePlay, handlePause, handleStop } = useChrono()

  return (
    <div>
      Chrono : {seconds}s
      <div>
        <button onClick={handlePlay}>Play</button>
        <button onClick={handlePause}>Pause</button>
        <button onClick={handleStop}>Stop</button>
      </div>
    </div>
  )
}
