import { Provider as ReduxProvider } from 'react-redux'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import ThemeProvider from '../contexts/ThemeContext'
import { store } from '../store'
import IndexPage from './pages/IndexPage'
import PokemonsPage from './pages/PokemonsPage'
import SigninPage from './pages/SigninPage'
import SignupPage from './pages/SignupPage'
import SignupSuccessPage from './pages/SignupSuccessPage'
import TestsPage from './pages/TestsPage'
import Layout from './templates/Layout'

export default function App() {
  return (
    <ReduxProvider store={store}>
      <ThemeProvider>
        <Router>
          <Routes>
            <Route path="/" element={<Layout />}>
              <Route path="/" element={<IndexPage />} />
              <Route path="tests" element={<TestsPage />} />
              <Route path="pokemons" element={<PokemonsPage />} />
              <Route path="signin" element={<SigninPage />} />
              <Route path="signup" element={<SignupPage />} />
              <Route path="signup-success" element={<SignupSuccessPage />} />
            </Route>
          </Routes>
        </Router>
      </ThemeProvider>
    </ReduxProvider>
  )
}
