export default function SignupSuccessPage() {
  return (
    <>
      <h1>Inscription réussie !</h1>
      <p>Vous allez recevoir un email de confirmation.</p>
    </>
  )
}
