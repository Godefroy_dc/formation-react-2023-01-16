import { FormEventHandler, useState } from 'react'
import StyledForm from '../atoms/StyledForm'

export default function SigninPage() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit: FormEventHandler = (event) => {
    event.preventDefault()
    alert(`Connexion de ${email} avec le mot de passe ${password}...`)
  }

  return (
    <StyledForm onSubmit={handleSubmit}>
      <h1>Connexion</h1>

      <label>
        <span>Email :</span>
        <input
          type="email"
          required
          value={email}
          onChange={(event) => setEmail(event.target.value)}
        />
      </label>

      <label>
        <span>Mot de passe :</span>
        <input
          type="password"
          required
          value={password}
          onChange={(event) => setPassword(event.target.value)}
        />
      </label>

      <button>Se connecter</button>
    </StyledForm>
  )
}
