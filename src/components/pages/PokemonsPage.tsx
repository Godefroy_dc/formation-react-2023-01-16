import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../store'
import { fetchPokemonsList } from '../../store/pokemon'

export default function PokemonsPage() {
  const { entries, loading, error } = useSelector(
    (state: RootState) => state.pokemon
  )
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch<any>(fetchPokemonsList())
  }, [])

  return (
    <ul>
      {loading && <li>Loading...</li>}
      {error && <li>Error: {error}</li>}
      {entries &&
        entries.map((pokemon) => <li key={pokemon.id}>{pokemon.name}</li>)}
    </ul>
  )
}
