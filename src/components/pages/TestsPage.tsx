import { useState } from 'react'
import { FaStar } from 'react-icons/fa'
import ControlledCounter from '../atoms/ControlledCounter'
import Counter from '../atoms/Counter'
import CounterButton from '../atoms/CounterButton'
import ReduxCounter from '../atoms/ReduxCounter'
import ScrollContainer from '../atoms/ScrollContainer'
import SuperButton from '../atoms/SuperButton'
import Chrono from '../molecules/Chrono'
import PokemonCard from '../organisms/PokemonCard'

export default function TestsPage() {
  const [pokemonId, setPokemonId] = useState(25)

  return (
    <>
      <SuperButton icon={<FaStar />} onClick={() => console.log('super click')}>
        Clique-moi
      </SuperButton>
      <hr />
      <CounterButton />
      <Counter onChange={(count) => console.log('[App] Counter:', count)} />
      <ReduxCounter />
      <hr />
      <Chrono />
      <hr />
      <ControlledCounter value={pokemonId} onChange={setPokemonId} />
      <PokemonCard id={pokemonId} />
      <hr />
      <ScrollContainer />
    </>
  )
}
