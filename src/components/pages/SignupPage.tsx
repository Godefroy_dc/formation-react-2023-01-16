import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { z } from 'zod'
import StyledForm from '../atoms/StyledForm'

// Gender values
const genders = ['F', 'M', 'O'] as const

// Translations for genders
const gendersTranslations: Record<typeof genders[number], string> = {
  F: 'Mme',
  M: 'M.',
  O: 'Autre',
}

const schema = z
  .object({
    gender: z.enum(genders),
    name: z.string().min(3),
    email: z.string().email(),
    password: z.string().min(8),
    passwordConfirm: z.string().min(8),
    birthdate: z.string().optional(),
    cgu: z.boolean(),
  })
  .refine((data) => data.password === data.passwordConfirm, {
    message: "Passwords don't match",
    path: ['passwordConfirm'],
  })
  .refine((data) => data.cgu, {
    message: 'Please accept conditions',
    path: ['cgu'],
  })

type Values = z.infer<typeof schema>

// Without validation, we could have used this:
// interface Values {
//   gender: typeof genders[number]
//   name: string
//   email: string
//   password: string
//   passwordConfirm: string
//   birthdate?: string
//   cgu: boolean
// }

export default function SignupPage() {
  const navigate = useNavigate()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Values>({
    resolver: zodResolver(schema),
  })

  const onSubmit = (values: Values) => {
    alert(JSON.stringify(values, null, 2))
    navigate('/signup-success')
  }

  return (
    <StyledForm onSubmit={handleSubmit(onSubmit)}>
      <h1>Inscription</h1>

      <label>
        <span>Nom :</span>
        <select {...register('gender')}>
          {genders.map((g) => (
            <option key={g} value={g}>
              {gendersTranslations[g]}
            </option>
          ))}
        </select>
        <input
          type="text"
          placeholder="Prénom Nom"
          {...register('name')}
          aria-invalid={errors.name ? 'true' : 'false'}
        />
        {errors.name && <p role="alert">{errors.name.message}</p>}
      </label>

      <label>
        <span>Email :</span>
        <input
          type="email"
          placeholder="---@---.--"
          {...register('email')}
          aria-invalid={errors.email ? 'true' : 'false'}
        />
        {errors.email && <p role="alert">{errors.email.message}</p>}
      </label>

      <label>
        <span>Mot de passe :</span>
        <input
          type="password"
          {...register('password')}
          aria-invalid={errors.password ? 'true' : 'false'}
        />
        {errors.password && <p role="alert">{errors.password.message}</p>}
      </label>

      <label>
        <span>Confirmer mot de passe :</span>
        <input
          type="password"
          {...register('passwordConfirm')}
          aria-invalid={errors.passwordConfirm ? 'true' : 'false'}
        />
        {errors.passwordConfirm && (
          <p role="alert">{errors.passwordConfirm.message}</p>
        )}
      </label>

      <label>
        <span>Date de naissance :</span>
        <input type="date" {...register('birthdate')} />
      </label>

      <label>
        <input type="checkbox" {...register('cgu')} />
        J'accepte les <a href="#">CGU</a>
        {errors.cgu && <p role="alert">{errors.cgu.message}</p>}
      </label>

      <button>Je m'inscris</button>
    </StyledForm>
  )
}
