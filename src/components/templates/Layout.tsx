import { Outlet } from 'react-router-dom'
import styled from 'styled-components'
import { Header } from '../organisms/Header'

const StyledLayout = styled.div`
  border-left: 1px solid #ccc;
  border-right: 1px solid #ccc;
  margin: 0 auto;
  max-width: 800px;
  min-height: 100vh;

  section {
    padding: 2em;
    *:first-child {
      margin-top: 0;
    }
  }
`

export default function Layout() {
  return (
    <StyledLayout>
      <Header />
      <section>
        <Outlet />
      </section>
    </StyledLayout>
  )
}
