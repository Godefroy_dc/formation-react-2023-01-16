import { useCallback, useEffect, useRef, useState } from 'react'

export function useChrono() {
  const [ms, setMs] = useState(0)
  const msRef = useRef(0) // Usefull to optimize play
  const startRef = useRef(0)
  const intervalRef = useRef(0)

  const play = useCallback(() => {
    if (intervalRef.current) return
    startRef.current = new Date().getTime() - msRef.current
    intervalRef.current = setInterval(() => {
      setMs(new Date().getTime() - startRef.current)
    }, 100)
  }, [])

  const pause = useCallback(() => {
    clearInterval(intervalRef.current)
    intervalRef.current = 0
  }, [])

  const stop = useCallback(() => {
    pause()
    setMs(0)
  }, [pause])

  // Clear interval at unmount
  useEffect(() => () => clearInterval(intervalRef.current), [])

  // Update msRef at each ms change
  useEffect(() => {
    msRef.current = ms
  }, [ms])

  return { ms, play, pause, stop }
}
