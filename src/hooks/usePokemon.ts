import { useEffect, useState } from 'react'

export interface PokemonData {
  id: number
  name: string
  height: number
  weight: number
  sprites: {
    front_default: string
  }
}

export function usePokemon(id: number) {
  const [data, setData] = useState<PokemonData | undefined>()
  const [error, setError] = useState<string | undefined>()
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    // Reset states each time id changes
    setData(undefined)
    setError(undefined)
    setLoading(true)

    // Fetch Pokemon data
    fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
      .then((result) => {
        // Handle HTTP errors
        if (!result.ok) throw new Error('Pokemon not found')
        return result.json()
      })
      // Success -> update data
      .then((result) => setData(result))
      // Error -> update error
      .catch((error) => setError(error.message))
      // Finally -> stop loading
      .finally(() => setLoading(false))
  }, [id])

  return { data, error, loading }
}
