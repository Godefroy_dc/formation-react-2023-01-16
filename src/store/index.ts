import { configureStore } from '@reduxjs/toolkit'
import { counterReducer } from './counter'
import { pokemonReducer } from './pokemon'

// Create a Redux store holding the state of your app.
// Its API is { subscribe, dispatch, getState }.
export const store = configureStore({
  reducer: {
    counter: counterReducer,
    pokemon: pokemonReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
