import { Action } from 'redux'

const ACTION_INCREMENT = 'counter/incremented'
const ACTION_DECREMENT = 'counter/decremented'

export function counterReducer(state = { value: 0 }, action: Action) {
  switch (action.type) {
    case ACTION_INCREMENT:
      return { value: state.value + 1 }
    case ACTION_DECREMENT:
      return { value: state.value - 1 }
    default:
      return state
  }
}

export function increment() {
  return { type: ACTION_INCREMENT }
}

export function decrement() {
  return { type: ACTION_DECREMENT }
}
