import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface PokemonEntry {
  id: number
  name: string
}

export interface PokemonsState {
  entries: PokemonEntry[]
  loading: boolean
  error: string | undefined
}

const initialState: PokemonsState = {
  entries: [],
  loading: false,
  error: undefined,
}

// Extract Id from URL
// URL example: https://pokeapi.co/api/v2/pokemon/35/
const pokemonUrlToId = (url: string) =>
  parseInt(url.match(/\/(\d+)\/$/)?.[1] || '0', 10)

// API response type for fetchAll
interface PokemonsAPIResponse {
  count: number
  next: string
  previous: null
  results: {
    name: string
    url: string
  }[]
}

// Fetch pokemons list (id+name)
export const fetchPokemonsList = createAsyncThunk(
  'pokemon/fetchAll',
  async () => {
    const result = await fetch('https://pokeapi.co/api/v2/pokemon/?limit=999')
    if (!result.ok) throw new Error('Error fetching pokemons list')
    const json: PokemonsAPIResponse = await result.json()
    return json.results.map((entry) => ({
      id: pokemonUrlToId(entry.url),
      name: entry.name,
    }))
  }
)

const pokemonSlice = createSlice({
  name: 'pokemon',
  initialState,
  reducers: {},
  extraReducers: {
    // Loading list...
    [fetchPokemonsList.pending.type]: (state) => {
      state.loading = true
      state.error = undefined
      state.entries = []
    },

    // List fetched!
    [fetchPokemonsList.fulfilled.type]: (
      state,
      action: PayloadAction<PokemonEntry[]>
    ) => {
      state.loading = false
      state.entries = action.payload
    },

    // Error loading list
    [fetchPokemonsList.rejected.type]: (state, action) => {
      state.loading = false
      state.error = action.error.message
    },
  },
})

export const {} = pokemonSlice.actions
export const pokemonReducer = pokemonSlice.reducer
