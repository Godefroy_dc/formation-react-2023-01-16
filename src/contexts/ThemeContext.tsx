import { createContext, ReactNode, useEffect, useState } from 'react'

type Themes = 'light' | 'dark'

interface ThemeContextValue {
  theme: Themes
  setTheme: (theme: Themes) => void
}

export const ThemeContext = createContext<ThemeContextValue>({
  theme: 'light',
  setTheme: () => console.warn('Missing ThemeContext provider'),
})

export default function ThemeProvider({ children }: { children: ReactNode }) {
  const [theme, setTheme] = useState<Themes>('light')

  // Change <body> class
  useEffect(() => {
    const { classList } = document.body
    if (theme === 'light') {
      classList.remove('dark')
    } else {
      classList.add('dark')
    }
  }, [theme])

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}
